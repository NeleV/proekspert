package textfiles;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.*;
import java.io.IOException;
import java.lang.String;
import java.util.Map.Entry;
import java.util.stream.Collectors;


public class Main<data> {

    public static void main(String[] args) throws IOException {

        String file_name = "src/Proovitekst.txt";
        ReadFile file = new ReadFile(file_name);                //Sets up new ReadFile object called file

        String[] tekst = file.OpenFile();                        //Imported txt file to StringArray nimega tekst


        for (int i = 0; i < tekst.length; i++) {                      //tsükkel võtab läbi iga rea(Stringi)
            String[] yksikudSonad = tekst[i].split(" ");        //Lisab tühiku kaupa eraldatud sõnad yksikudSonad massiivi

            ArrayList<String> täheYhendid = new ArrayList<>();     //Loon uue massiivi kuhu salvestan täheühendid
            for (String yksYhend : yksikudSonad) {                       //Tsükkliga käin läbi iga täheühendi massiivist yksikudSonad
                for (int j = 0; j <= yksYhend.length(); j++) {
                    for (int n = 4; n <= yksYhend.length(); n++) {          //Määran muutuja n väärtuseks 4. Niikaua kui n on väikesem kui yksYhend (sõna) pikkus, tsükkel kordub.
                        if (j + n <= yksYhend.length()) {
                            täheYhendid.add(yksYhend.substring(j, j + n));
                        }
                    }
                }
            }

            HashMap<String, Integer> sagedus = new HashMap<>();          //Hashmap nimega sagedus, võtab key-ks iga täheühendi ja väärtuseks (integer) mitu korda see täheühend tekstis esineb
            for (String yksYhend : täheYhendid) {
                if (sagedus.containsKey(yksYhend)) {
                    int value = sagedus.get(yksYhend);
                    value++;
                    sagedus.put(yksYhend, value);
                } else {
                    sagedus.put(yksYhend, 1);
                }
            }

            //  System.out.println("Sagedus: " + sagedus);

            int koikSagedusedKokku = 0;
            for (Integer value : sagedus.values()) {
                koikSagedusedKokku += value;
            }

            //Tabeli "laadne" vorm
            System.out.println("-----------------------------------------------------------------------------");
            System.out.printf("TÄHEÜHEND    SAGEDUS");
            System.out.println();
            System.out.println("-----------------------------------------------------------------------------");

            Map<String, Integer> sorteeritudSageduseJargi = sorteeriKahanevalt(sagedus);

            int f = 0;
            for (Entry<String, Integer> yksYhend : sorteeritudSageduseJargi.entrySet()) {
                if (f > 9) break;
                float frequency = (float) (100 * yksYhend.getValue()) / koikSagedusedKokku;

                System.out.println(yksYhend.getKey() + "      " + frequency + "%");

                f++;


                double sageduseMoot = frequency;            //Horisontaalne bar chart tabeli sees
                String nool = yksYhend + ": ";
                for (int k = 0; k < sageduseMoot; k++) {
                    nool += "=";
                }
                nool += ">";
                System.out.println(nool);
            }
            
        }
    }

    public static Map<String, Integer> sorteeriKahanevalt(Map<String, Integer> silpideSagedus) {
        return silpideSagedus.entrySet()
                .stream()
                .sorted((Entry.<String, Integer>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

    }
}

/*
TESTJUHTUMID:

Testjuhtum1:
Tekstifail koosneb kahest pikast sõnast: allabellakella elevandiluurannik


Tulemuseks annab 10 erinevat kombinatsiooni:
ella: 1.1695906%
ndilu: 0.5847953%
uurannik: 0.5847953%
abellakell: 0.5847953%
elevandiluur: 0.5847953%
diluuranni: 0.5847953%
eleva: 0.5847953%
evandiluurannik: 0.5847953%
vandiluurannik: 0.5847953%
urannik: 0.5847953%


Testjuhtum2:
Tekstifailis on mitmerealine tekst.

Tulemuseks annab pika nimekirja esinevate kombiantsioonigega ning nende sageduse. Programm loeb ka punkte täheühikuteks.

Testjuhtum3:
Tekstifailis on üks 3-täheline sõna.

Tulemust ei ole, kuna see ei vasta tingimustele.

Testjuhtum4:
Tekstifailis on: alla bella

Tulemuseks on 4 ühendit mis esinevad kõik 25% kordadest

Testjuhtum5:
Tesktifailis on 4-ühikuslised ja suuremad sõnad ja üks või rohkem väikesemat

Tulemus: väikesemaid sõnu ei loeta.

Testjuhtum6:
Failis on kaks Stringi: "alla bella kella" ja "kuhu lähed täna"

Tulemus: arvestab sisse ka " (jutumärgid) kui täheühikut. Muidu toimib.

 */