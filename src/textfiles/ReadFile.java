package textfiles;

import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class ReadFile {         //Uus klass, mis tegeleb faili lugemisega

    private String path;

    public ReadFile(String file_path) {     //Konstruktor
        path = "src/Proovitekst.txt";         //tekstifaili aadress
    }

    public String[] OpenFile() throws IOException {     //Loon meetodi, mis tagastab koodiridu tekstifailist

        FileReader tekstiLugeja = new FileReader(path);         //Filereader nimega tekstiLugeja, mis loeb varasemalt määratud aadresslie antud faili
        BufferedReader textReader = new BufferedReader(tekstiLugeja);

        int numberOfLines = readLines( );          //Setting up an array for each line of text
        String[ ] textData = new String[numberOfLines];

        int i;

        for (i=0; i < numberOfLines; i++) {         //creating a loop that puts lines of text into array
            textData[i] = textReader.readLine( );   //accesses lines of text and stores them in array

        }
        textReader.close();
        return textData;
    }

    int readLines() throws IOException {
        FileReader file_to_read = new FileReader(path);
        BufferedReader bf = new BufferedReader(file_to_read);

        String aLine;
        int numberOfLines = 0;

        while ((aLine = bf.readLine()) != null) {
            numberOfLines++;
        }
        bf.close();
        return numberOfLines;
    }
}
